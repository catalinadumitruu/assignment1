function distance(first, second){
	if(!(first instanceof Array)){
		throw new Error("InvalidType");
	}

	if(!(second instanceof Array)){
		throw new Error("InvalidType");
	}

	if(first == "" ){
		return 0;
	}

	if(second == ""){
		return 0;
	}

	let distance = 0;
    
	
	let newF = first.filter((item, i, arr) => {
		return arr.indexOf(item) == i;
	});
	
	let newS = second.filter((item, i, arr) => {
		return arr.indexOf(item) == i;
	});

	for(itemF of newF){
		for(itemS of newS){
			if(itemF !== itemS){
				distance++;
			}
		}
	}

	for(itemF of newF){
		for(itemS of newS){
			if(typeof itemF !== typeof itemS){
				distance++;
			}
		}
	}

	return distance;
}


module.exports.distance = distance